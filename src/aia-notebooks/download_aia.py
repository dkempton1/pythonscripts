
import sys
import os
import time
import urllib
import pdb
import drms
import random
import glob
import tarfile
import traceback
import threading
import urllib.request

import numpy as np, pandas as pd
from multiprocessing import Pool
from random import randint

DRMS_CLIENT = 'mschuh@cs.gsu.edu'

save_dir = './1'

def download_helper(startDate, nDays, wavelength):

	attemptCount = 0
	hasComplete = False
	while attemptCount < 3 and not hasComplete:
		try:
			c = drms.Client(verbose=True)
			si = {
				94: c.info('aia.lev1_euv_12s'),
				131: c.info('aia.lev1_euv_12s'),
				171: c.info('aia.lev1_euv_12s'),
				193: c.info('aia.lev1_euv_12s'),
				211: c.info('aia.lev1_euv_12s'),
				304: c.info('aia.lev1_euv_12s'),
				335: c.info('aia.lev1_euv_12s'),
				1600: c.info('aia.lev1_uv_24s'),
				1700: c.info('aia.lev1_uv_24s')
			}[wavelength]

			
			tsTmp = startDate
			if tsTmp != '':
				tsTmp = "{0}T00:00:00_TAI/{1}d@2h".format(startDate, nDays)
		
			#print(tsTmp)
			q = {
				94: 'aia.lev1_euv_12s[{0}][{1}]',
				131: 'aia.lev1_euv_12s[{0}][{1}]',
				171: 'aia.lev1_euv_12s[{0}][{1}]',
				193: 'aia.lev1_euv_12s[{0}][{1}]',
				211: 'aia.lev1_euv_12s[{0}][{1}]',
				304: 'aia.lev1_euv_12s[{0}][{1}]',
				335: 'aia.lev1_euv_12s[{0}][{1}]',
				1600: 'aia.lev1_uv_24s[{0}][{1}]',
				1700: 'aia.lev1_uv_24s[{0}][{1}]'
			}[wavelength].format(tsTmp, wavelength)
			
			print(q)
			k = c.query(q, key='T_REC')
			print('  Total records found: ', len(k))
			
			if(len(k)>0):
				
				maxReq = 25000
				r = c.export(q+'{image}', method='ftp-tar', protocol='fits', n=maxReq, email=DRMS_CLIENT)

				print('Starting download...')
				time.sleep(10)
				start = time.time()
				tryCnt = 0
				done = False
				while tryCnt < 10 and not done:

					while (not r.has_finished()) and (not r.has_failed()) :
						r.wait()

					tarurl = r.urls['url'][0]
					filename = r.urls['filename'][0]
					save_loc = save_dir+'/'+filename
					print(tarurl)
					print(save_loc)

					try:
						#make request and set timeout for no data to 500 seconds and enable streaming		
						t_out = 500
						with urllib.request.urlopen(tarurl, timeout=t_out) as resp:
							#Open output file and make sure we write in binary mode
							with open(save_loc + '.part','wb') as fh:
								#walk through the request response in chunks of 1MiB
								while True:
									chunk = resp.read(1024*1024)
									if not chunk:
										break
									#Write the chunk to the file
									fh.write(chunk)

						#Rename the file
						os.rename(save_loc + '.part', save_loc)
						#r.download(save_dir)
						done = True
					except Exception:
						print(traceback.format_exc())
						time.sleep(5)
						tryCnt = tryCnt + 1

				
				end = time.time()
				tdelt = int((end-start)/60.0)
				print("Finished in: %f minutes." % tdelt )

			hasComplete = True
		except Exception:
			print(traceback.format_exc())
			attemptCount = attemptCount + 1

def check_for_tar(save_dir):
	files = []
	files.extend(glob.glob(save_dir + "/*.tar"))
	
	for fname in files:
		if(fname.endswith("tar")):
			try:
				tar = tarfile.open(fname, "r:")
				tar.extractall(save_dir)
				tar.close()
			except Exception:
				print(traceback.format_exc())
			os.remove(fname)

	types = ('*.txt','*.html', '*.json', '*.drmsrun', '*.qsub', '*.tar.*')
	files = []
	for ftype in types:
		files.extend(glob.glob(save_dir + "/" + ftype))

	for f in files:
		os.remove(f)
	
#//////////////////////////////////////////////
def help():
	
	argstr = """
	python download_aia.py [mode] {args}
		
		1: Download a specific wavelength
			args: [start time yyyy-mm-dd] [days to download] [aia wavelength]
				Supported wavelengths: 94, 131, 171, 193, 211, 304, 335, 1600, 1700
		2: Download all wavelengths at once
			args: [start time yyyy-mm-dd] [days to download] 
		
	
	"""
	print(argstr)

def worker_funct(data):
	print(data)
	download_helper(data[0],data[1],data[2])

def run_mainnew():

	if len(sys.argv) < 3:
		help()
		exit()

	opt = int(sys.argv[1])
	days = int(sys.argv[3])
	if opt == 1:
		wave = int(sys.argv[4])
		download_helper(sys.argv[2],days,wave)
	elif opt == 2:
		waves = [94, 131, 171, 193, 211, 304, 335, 1600, 1700]
		data = ()
		for wave in waves:
			data = data + ([sys.argv[2], days, wave],)

		print (data)
		pool = Pool(processes=7)
		pool.map(worker_funct, data)

	check_for_tar(save_dir)

if __name__ == "__main__":
	run_mainnew()
	
