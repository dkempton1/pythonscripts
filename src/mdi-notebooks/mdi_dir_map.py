import os
import re
import sys


class MDIDirMap():

	def __init__(self, rawDataDirectory):
		self._dataDirectory = rawDataDirectory
		self._fileMap = {}
		
		subdirs = [(dr, os.path.join(self._dataDirectory, dr)) for dr in os.listdir(self._dataDirectory) if os.path.isdir(os.path.join(self._dataDirectory, dr))]
		
		for d in subdirs:
			self._fileMap[d[0]] = {}
			self.__getFilesInDir(d[1], self._fileMap[d[0]])
	
	def __find_files(self, path, file_filter):	
		ret_list = []
		
		for entry in os.scandir(path=path):
			if file_filter.match(entry.name) is not None:	
				ret_list.append(entry.name)
		
		return ret_list
	
	def __getFilesInDir(self, dirName, fileMap):
		subdirs = [ os.path.join(dirName, dr) for dr in os.listdir(dirName) if os.path.isdir(os.path.join(dirName, dr))]
		rx = re.compile(r'(^[0-9]{4}\.[0-9]{2}\.[0-9]{2}_).*.fits')
		for d in subdirs:
			filesList = self.__find_files(d + "/", rx)
			for file in filesList:
				mo = file[5:7]
				day = file[8:10]
				if mo in fileMap.keys():
					if day not in fileMap[mo].keys():
						fileMap[mo][day] = ([],d)
					fileMap[mo][day][0].append(file)
				else:
					fileMap[mo] = {}
					fileMap[mo][day] = ([], d)
					fileMap[mo][day][0].append(file)
	
	def findFileForName(self, fileName):
		year = fileName[0:4]
		mon = fileName[5:7]
		day = fileName[8:10]
		dirName = ""
		
		if year in self._fileMap.keys():
			monMap = self._fileMap[year]
			if mon in monMap.keys():
				dayMap = monMap[mon]
				if day in dayMap.keys():
					filesList = dayMap[day]
					for file in filesList[0]:
						if file.startswith(fileName):
							dirName = os.path.join(filesList[1], file)
							break
		
		return dirName