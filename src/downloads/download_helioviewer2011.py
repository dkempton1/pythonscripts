import os
import requests
import urllib.request
from lxml import html
from calendar import monthrange
from datetime import datetime, timedelta

downloadDir = "/datapool/data/AIA_JP2"

minCadence = 3

year = "2011"
waves = ["94", "131", "171", "193", "211", "304", "335", "1600", "1700"]
months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]

for month in months:
	for day in range(1, monthrange(int(year), int(month))[1] + 1):
		for wave in waves:
			downloadString = "https://helioviewer.org/jp2/AIA/" + wave + "/" + year + "/" + month + "/" + str(day).zfill(2)
			saveString = downloadDir + "/" + year + "/" + month + "/" + str(day).zfill(2) + "/" + wave

			page_as_string = requests.get(downloadString).content
			tree = html.fromstring(page_as_string)

			links = [e.text_content() for e in tree.iter() if e.tag == 'a']

			links2 = []
			for link in links:
				if link.endswith(".jp2"):
					links2.append(link)
			
			links2.sort()
			
			if not os.path.exists(saveString):
				os.makedirs(saveString)

			lastDownload = None

			for link in links2:

				linkSplit = link.rsplit('_')

				datetime.now()
				currentTime = datetime(int(linkSplit[0]), int(linkSplit[1]),
								  int(linkSplit[2]), int(linkSplit[4]),
								  int(linkSplit[5]))

				if lastDownload is None or currentTime >= lastDownload + timedelta(minutes = minCadence):

					filename = os.path.join(saveString, link)
					downloadLink = downloadString + "/" + link
					if not os.path.isfile(filename):
						try:
							print("Downloading: " + downloadLink)
							urllib.request.urlretrieve(downloadLink, filename)
							
						except Exception as inst:
							print(inst)
							print(' Encountered unknown error. Continuing.')
					lastDownload = currentTime
