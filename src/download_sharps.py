import sys
import os
import time
import drms
import glob
import tarfile
import traceback
import shutil
import urllib.request
import csv
import ast

from multiprocessing import Pool

DRMS_CLIENT = 'mschuh@cs.gsu.edu'

# BASE_DIR = "/home/dustin/Desktop/harps/data"
# BASE_DIR = "/home/dustin/sharps"
BASE_DIR = "/datapool/data/SHARPS"
# RAW_FILES_DIR = "/raw-data"
RAW_FILES_DIR = "/raw-sharps"
# HEADER_FILES_DIR = "/headers"
HEADER_FILES_DIR = "/sharps-headers"

# DIR_DL = './'
# DIR_DL = '/datapool/data/SHARPS/raw-sharps/'

# MISSING_DIR = "/home/dustin/Desktop/harps"
MISSING_DIR = "/home/dkempton"
#MISSING_FILE = "MissingSharpInfo.csv"
MISSING_FILE = "NEW_WrongData.csv"


def download_sharps_helper(sn, ts, save_dir):

	attemptCount = 0
	hasComplete = False
	while attemptCount < 5 and not hasComplete:
		try:
			c = drms.Client(verbose = True)

			# partial sharp query, use the supplied time stamp
			if not ts:
				tsTmp = ''
			else:
				tsTmp = ts + '_TAI'
			q = "hmi.sharp_cea_720s[%s][%s]" % (sn, tsTmp)
			print(q)

			k = c.query(q, key = 'HARPNUM, T_REC')
			print('  Total records found: ', len(k))

			tdelt = 0
			if(len(k) > 0):

				# now retrieve export data
				maxReq = 25000

				r = c.export(q + '{Br,Bt,Bp,Bitmap,Conf_disambig,magnetogram,Dopplergram,continuum,Bp_err,Bt_err}', method = 'ftp-tar', protocol = 'fits', n = maxReq, email = DRMS_CLIENT)

				print('Starting download...')
				time.sleep(10)
				start = time.time()
				tryCnt = 0
				done = False
				while tryCnt < 10 and not done:

					while (not r.has_finished()) and (not r.has_failed()) :
						r.wait(timeout=90)

					tarurl = r.urls['url'][0]
					filename = r.urls['filename'][0]
					save_loc = save_dir + '/' + filename
					print(tarurl)
					print(save_loc)

					try:

						# urllib.request.urlretrieve(tarurl, save_loc + '.part')
						# make request and set timeout for no data to 500 seconds and enable streaming
						t_out = 500
						with urllib.request.urlopen(tarurl, timeout = t_out) as resp:
							# Open output file and make sure we write in binary mode
							with open(save_loc + '.part', 'wb') as fh:
								# walk through the request response in chunks of 1MiB
								while True:
									chunk = resp.read(1024 * 1024)
									if not chunk:
										break
									# Write the chunk to the file
									fh.write(chunk)

						# Rename the file
						os.rename(save_loc + '.part', save_loc)
						# r.download(save_dir)
						done = True
					except Exception:
						print(traceback.format_exc())
						time.sleep(5)
						tryCnt = tryCnt + 1

				end = time.time()
				tdelt = int((end - start) / 60.0)
			print("Finished in: %f minutes." % tdelt)

			hasComplete = True
		except Exception as e:
			if isinstance(e, drms.DrmsExportError):
				download_sharps_helper2(sn, save_dir)
				hasComplete = True
			elif isinstance(e, urllib.error.HTTPError):
				print("urlErr: " + str(e.code))
			else:
				print(traceback.format_exc())
				attemptCount = attemptCount + 1

	return


def download_sharps_helper2(sn, save_dir):

	partSize = 100
	attemptCount = 0
	hasComplete = False
	while attemptCount < 5 and not hasComplete:
		try:
			c = drms.Client(verbose = True)

			# partial sharp query, use the supplied time stamp

			tsTmp = ''
			q = "hmi.sharp_cea_720s[%s][%s]" % (sn, tsTmp)
			print(q)

			k = c.query(q, key = 'HARPNUM, T_REC')
			print('  Total records found: ', len(k))

			tiaList = []
			for t in k.T_REC:
				tiaList.append(t)

			tdelt = 0

			if(len(k) > 0):

				numPartitions = len(tiaList) // partSize

				# Check to see how many parts were already downloaded since I screwed up once.
				files = []
				files.extend(glob.glob(save_dir + "/*.tar"))
				partStart = 0
				for fname in files:
					if(fname.endswith("tar")):
						partStart = partStart + 1

				for i in range(partStart, numPartitions + 1):
					tsTmp = ''
					if(i < numPartitions):
						for j in range(0, partSize):
							tsTmp = tsTmp + tiaList[j + i * partSize] + ', '
					else:
						j = i * partSize
						while (j < len(tiaList)):
							tsTmp = tsTmp + tiaList[j] + ', '
							j = j + 1

					q = "hmi.sharp_cea_720s[%s][%s]" % (sn, tsTmp)
					print(q)

					k = c.query(q, key = 'HARPNUM, T_REC')
					print('  Total records found: ', len(k))
					# now retrieve export data
					maxReq = 25000

					r = c.export(q + '{Br,Bt,Bp,Bitmap,Conf_disambig,magnetogram,Dopplergram,continuum,Bp_err,Bt_err}', method = 'ftp-tar', protocol = 'fits', n = maxReq, email = DRMS_CLIENT)

					print('Starting download...')
					time.sleep(10)
					start = time.time()
					tryCnt = 0
					done = False
					while tryCnt < 10 and not done:

						while (not r.has_finished()) and (not r.has_failed()) :
							r.wait(timeout=90)

						tarurl = r.urls['url'][0]
						filename = r.urls['filename'][0]
						save_loc = save_dir + '/' + filename
						print(tarurl)
						print(save_loc)

						try:

							# make request and set timeout for no data to 500 seconds and enable streaming
							t_out = 500
							with urllib.request.urlopen(tarurl, timeout = t_out) as resp:
								# Open output file and make sure we write in binary mode
								with open(save_loc + '.part', 'wb') as fh:
									# walk through the request response in chunks of 1MiB
									while True:
										chunk = resp.read(1024 * 1024)
										if not chunk:
											break
										# Write the chunk to the file
										fh.write(chunk)

							# Rename the file
							os.rename(save_loc + '.part', save_loc)
							# r.download(save_dir)
							done = True
						except Exception:
							print(traceback.format_exc())
							time.sleep(5)
							tryCnt = tryCnt + 1

				end = time.time()
				tdelt = int((end - start) / 60.0)
			print("Finished in: %f minutes." % tdelt)

			hasComplete = True
		except Exception:
			print("Outer")
			print(traceback.format_exc())
			attemptCount = attemptCount + 1

	return


def check_for_tar(save_dir):
	files = []
	files.extend(glob.glob(save_dir + "/*.tar"))

	for fname in files:
		if(fname.endswith("tar")):
			try:
				tar = tarfile.open(fname, "r:")
				tar.extractall(save_dir)
				tar.close()
			except Exception:
				print(traceback.format_exc())
			os.remove(fname)

	types = ('*.txt', '*.html', '*.json', '*.drmsrun', '*.qsub', '*.tar.*')
	files = []
	for ftype in types:
		files.extend(glob.glob(save_dir + "/" + ftype))

	for f in files:
		os.remove(f)


def get_ts_list_from_dir(save_dir):
	files = []
	files.extend(glob.glob(save_dir + "/*.fits"))

	tsList = {}
	for fname in files:
		if(fname.endswith("fits")):
			parts = fname.split('.')
			ts = parts[4][0:15]
			tsList[ts] = ts

	return tsList


def check_missing_ts_exist(sn, missingTsList):

	foundMissing = []
	attemptCount = 0
	hasComplete = False
	while attemptCount < 5 and not hasComplete:
		try:
			c = drms.Client(verbose = True)

			# partial sharp query, use the supplied time stamp
			tsTmp = ''

			q = "hmi.sharp_cea_720s[%s][%s]" % (sn, tsTmp)
			print(q)

			k = c.query(q, key = 'HARPNUM, T_REC')
			print('  Total records found: ', len(k))
			for rec in k.T_REC:
				if rec[0:16] in missingTsList:
					print("Found: " + rec[0:16])
					for rec2 in k.T_REC:
						foundMissing.append(rec2[0:16])
					break

			hasComplete = True
		except Exception:
			print(traceback.format_exc())
			attemptCount = attemptCount + 1

	return foundMissing


def download_sharps_collection(sn, save_dir):

	if not os.path.exists(save_dir):
		os.mkdir(save_dir)

	download_sharps_helper(sn, '', save_dir)
	check_for_tar(save_dir)


def download_sharps_keysonly(sn):

	fname = BASE_DIR + HEADER_FILES_DIR + "/" + sn + '.txt'

	if os.path.isfile(fname):
		os.remove(fname)

	c = drms.Client(verbose = True)
	q = "hmi.sharp_cea_720s[%s][]" % (sn)
	print(q)

	success = False
	while success == False:

		try:
			k = c.query(q, key = '**ALL**')
		except urllib.error.URLError:
			print('\t urlib query error.. retrying..')
			time.sleep(30)
		except:
			print('\t other query error.. retrying..')
			time.sleep(30)
		else:
			success = True

	if 'CODEVER7' in k.columns:
		k['CODEVER7'] = k['CODEVER7'].str.replace('\n', '\\n')

	k.to_csv(fname, sep = '\t', index = False)

########################################################################


def help():

	# for mainnew()
	argstr = """
	python download_sharps.py [mode] {args}
		
		1: Download an entire sharp
			args: [sharpnum]
		2: Download range of sharps
			args: [begin sharpnum] [end sharpnum]
		3: Download sharps listed in a file deleting directory if alread there first
			args: [file location]
		4: Check to see if files exist for time stamps of missing data in file
		   Download the directory again if the data exists but was not downloaded
		5: Uses MissingSharpInfo.csv from verify script to redownload SHARP 
		   numbers that have missing time steps according to verify script.
		6: Uses MissingSharpInfo.csv from verify script to redownload headers only.
			
	"""
	print(argstr)


def run_mainnew():

	if len(sys.argv) == 1:
		help()
		exit()

	opt = int(sys.argv[1])

	if opt == 1:
		# download
		sn = sys.argv[2]

		fdir = BASE_DIR + RAW_FILES_DIR + "/" + sn
		if not os.path.exists(fdir):
			os.mkdir(fdir)

		download_sharps_collection(int(sn), fdir)

	elif opt == 2:
		# loop range of sn.. [from, to)
		sn_from = int(sys.argv[2])
		sn_to = int(sys.argv[3])  # exclusive

		pool = Pool(processes = 7)
		pool.map(worker_funct, range(sn_from, sn_to))

	elif opt == 3:
		download_from_file(sys.argv[2])
	elif opt == 4:
		check_from_file(sys.argv[2])
	elif opt == 5:
		use_missing_data_file()
	elif opt == 6 :
		use_missing_data_file_headers()
	else:
		print('invalid mode')


def worker_funct(idVal):

	fdir = BASE_DIR + RAW_FILES_DIR + "/" + str(idVal)
	if not os.path.exists(fdir):
		os.mkdir(fdir)

	try:
		download_sharps_collection(idVal, fdir)
	except Exception:
		print(traceback.format_exc())


def worker_funct_delsharp_first(idVal):

	fdir = BASE_DIR + RAW_FILES_DIR + "/" + str(idVal)
	if os.path.exists(fdir):
		shutil.rmtree(fdir)

	if not os.path.exists(fdir):
		os.mkdir(fdir)

	try:
		download_sharps_collection(idVal, fdir)
	except Exception:
		print(traceback.format_exc())


def worker_funct3(missingTs):

	sharpId = missingTs.sharpId
	missingTsList = missingTs.tsList

	try:
		found = check_missing_ts_exist(sharpId, missingTsList)

		fdir = BASE_DIR + RAW_FILES_DIR + "/" + str(sharpId)

		if len(found) > 0:
			if os.path.exists(fdir):
				tsInDir = get_ts_list_from_dir(fdir)
				dirTs = TSObj(sharpId)
				for ts in tsInDir:
					dirTs.add_ts(ts)

				needToReDo = []
				for ts in found:
					if ts not in dirTs.tsList:
						needToReDo.append(ts)

				if len(needToReDo) > 0:
					print("ReDownload SharpId: " + str(sharpId))

					if len(needToReDo) > 20:
						if os.path.exists(fdir):
							shutil.rmtree(fdir)

						download_sharps_collection(sharpId, fdir)
					else:
						if not os.path.exists(fdir):
							os.mkdir(fdir)

						for ts in needToReDo:
							download_sharps_helper(sharpId, ts, fdir)
							check_for_tar(fdir)
			else:
				os.mkdir(fdir)
				download_sharps_collection(sharpId, fdir)

	except Exception:
		print(traceback.format_exc())


def download_from_file(fileName):
	hdata = ()
	with open(fileName) as csvFile:
		valueReader = csv.reader(csvFile)
		for row in valueReader:
			hdata = hdata + (int(row[0]),)

	pool = Pool(processes = 7)
	pool.map(worker_funct_delsharp_first, hdata)


class TSObj:

	def __init__(self, sharpId):
		self.sharpId = sharpId
		self.tsList = []

	def add_ts(self, ts):
		tss = ts[0:4] + '.' + ts[4:6] + '.' + ts[6:8] + '_' + ts[9:11] + ':' + ts[11:13]
		self.tsList.append(tss)

	def __str__(self):
		return str(self.sharpId) + " , " + str(self.tsList)


def check_from_file(fileName):
	hdata = ()
	with open(fileName) as csvFile:
		valueReader = csv.reader(csvFile)
		next(valueReader, None)  # skip the header
		for row in valueReader:
			missingTs = TSObj(int(row[0]))
			for i in range(0, int(row[1])):
				missingTs.add_ts(row[i + 2])
			hdata = hdata + (missingTs,)

	pool = Pool(processes = 4)
	pool.map(worker_funct3, hdata)


def use_missing_data_file():

	hdata = ()

	with open(MISSING_DIR + "/" + MISSING_FILE) as csvFile:
		valueReader = csv.reader(csvFile, delimiter = '\t')
		next(valueReader, None)  # skip the header
		for row in valueReader:
			sharpNum = row[0]
			misstingTimesList = ast.literal_eval(row[1])
			if len(misstingTimesList) > 0:
				if misstingTimesList[0] == "All 2" or len(misstingTimesList) > 3:
					hdata = hdata + (sharpNum,)

	pool = Pool(processes = 7)
	pool.map(worker_funct_delsharp_first, hdata)


def use_missing_data_file_headers():

	hdata = ()

	with open(MISSING_DIR + "/" + MISSING_FILE) as csvFile:
		valueReader = csv.reader(csvFile, delimiter = '\t')
		next(valueReader, None)  # skip the header
		for row in valueReader:
			sharpNum = row[0]
			misstingTimesList = ast.literal_eval(row[1])
			if(len(misstingTimesList) > 0):
				hdata = hdata + (sharpNum,)

	pool = Pool(processes = 7)
	pool.map(download_sharps_keysonly, hdata)


if __name__ == "__main__":
	run_mainnew()

