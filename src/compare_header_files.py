import os
import csv
import math
import glob
import numpy as np
import pandas as pd

from multiprocessing import Pool
import traceback

# BASE_DIR = "/home/dustin/Desktop/harps/data"
# BASE_DIR = "/home/dustin/sharps"
BASE_DIR = "/datapool/data/SHARPS"

NEW_HEADER_FILES_DIR = "/new-data2"
HEADER_FILES_DIR = "/sharps-headers"

OUTPUT_DIR = "/home/dkempton"
WRONGDATA_FILE = "WrongData.csv"

IDXES = ['TOTUSJH', 'TOTPOT', 'TOTUSJZ', 'ABSNJZH', 'SAVNCPP',
	 'USFLUX', 'MEANPOT', 'SHRGT45', 'MEANSHR', 'MEANGAM', 'MEANGBT',
	 'MEANGBZ', 'MEANGBH', 'MEANJZH', 'MEANJZD', 'MEANALP']


def getNewHeaderNames():
	files = []
	files.extend(glob.glob(BASE_DIR + "/" + NEW_HEADER_FILES_DIR + "/*.csv"))
	filesWithExt = {}
	for file in files:
		filesWithExt[file.split("/")[-1].split(".")[0]] = file

	return filesWithExt

def getHeaderNames():
	files = []
	files.extend(glob.glob(BASE_DIR + "/" + HEADER_FILES_DIR + "/*.txt"))
	filesWithExt = {}
	for file in files:
		filesWithExt[file.split("/")[-1].split(".")[0]] = file

	return filesWithExt

def is_non_zero_file(fpath):
	return os.path.isfile(fpath) and os.path.getsize(fpath) > 0


def get_error_count(jsoc_header_path, calc_header_path):
	count = 0.0
	error = 0.0
	rows = 0.0

	errors = {}
	for idx in IDXES:
		errors[idx] = 0.0

	if(is_non_zero_file(jsoc_header_path) and is_non_zero_file(calc_header_path)):
		jsoc_header = pd.read_csv(jsoc_header_path, sep = '\t')
		calc_header = pd.read_csv(calc_header_path, sep = '\t', header=0)

		count = count + (jsoc_header.shape[0] * len(IDXES))
		rows = rows + jsoc_header.shape[0]
		for idx in IDXES:
			jsoc_col = jsoc_header[idx].values
			calc_col = calc_header[idx].values
			for i in range(0, jsoc_header.shape[0]):
				errorVal = abs(float(jsoc_col[i])-float(calc_col[i]))
				allowedErr = abs(float(jsoc_col[i])) * 0.02 

				if(errorVal > allowedErr):
					error = error + 1
					errors[idx] = errors[idx] + 1

	return [error, count, rows, errors]


def run_main():

	outHeader = ["ErrorCount", "DataCount", "RowCount", "Percentage", 'TOTUSJH', 'TOTPOT', 'TOTUSJZ', 'ABSNJZH', 'SAVNCPP',
	 'USFLUX', 'MEANPOT', 'SHRGT45', 'MEANSHR', 'MEANGAM', 'MEANGBT', 'MEANGBZ', 'MEANGBH', 'MEANJZH', 'MEANJZD', 'MEANALP']

	headers = getNewHeaderNames()

	pool = Pool(processes = 30)
	wrongDataCounts = pool.map(wrong_data_check,headers)

	
	errors = {}
	for idx in IDXES:
		errors[idx] = 0.0

	count = 0.0
	error = 0.0
	rows = 0.0
	
	for countTuple in wrongDataCounts:
		if countTuple is not None:
			error = error + countTuple[0]
			count = count + countTuple[1]
			rows = rows + countTuple[2]
			for idx in IDXES:
				errors[idx] = errors[idx] + countTuple[3][idx]

	print(error/count)

	with open(OUTPUT_DIR + "/" + WRONGDATA_FILE, 'w') as csvfile:
		csvout = csv.writer(csvfile, delimiter = '\t')
		csvout.writerow(outHeader)
		csvout.writerow([error, count, rows, (error/count)*100]+[errors[idx] for idx in IDXES])



def wrong_data_check(sharp):
	new_header_name = BASE_DIR + NEW_HEADER_FILES_DIR + "/" + sharp + ".csv"
	header_name = BASE_DIR + HEADER_FILES_DIR + "/" + sharp + ".txt"
	
	try:
		if os.path.isfile(header_name) and os.path.isfile(new_header_name):
			return get_error_count(header_name, new_header_name)
		else:
			return None
	except Exception as e:
		traceback.print_exc()
		return None


if __name__ == "__main__":
	run_main()

