
import sys
import os
import time
import urllib
import pdb
import csv
import ast
import drms
import random

from multiprocessing import Pool

DIR_DL = '/datapool/data/SHARPS/sharps-headers/'
DRMS_CLIENT = 'mschuh@cs.gsu.edu'

MISSING_DIR = "/home/dkempton"
MISSING_FILE = "NEW_WrongData.csv"


def download_sharps_keysonly2(sn):

	fname = DIR_DL + sn + '.txt'

	c = drms.Client(verbose = True)

	c.info('hmi.sharp_cea_720s')
	q = "hmi.sharp_cea_720s[%s][]" % (sn)
	print(q)

	success = False
	while success == False:

		try:
			k = c.query(q, key = '**ALL**')


		except urllib.error.URLError:
			print('\t urlib query error.. retrying..')
			time.sleep(30)
		except:
			print('\t other query error.. retrying..')
			time.sleep(30)
		else:
			success = True

	if 'CODEVER7' in k.columns:
		k['CODEVER7'] = k['CODEVER7'].str.replace('\n', '\\n')

	k.to_csv(fname, sep = '\t', index = False)


########################################################################
def write_linelist_seperators(linelist, fstr, sep = ','):

	f = open(fstr, 'w')
	for s in linelist:
		for i in range(len(s) - 1):
			f.write(repr(s[i]))
			f.write(sep)
		f.write(repr(s[-1]))
		f.write('\n')

	f.flush()
	f.close()


def help():
	argstr = """
    python download_sharps.py [mode] [dltype] {args}
        
        1: Download a slice of sharp data[start sharpnum] [end sharpnum]
        python driver_sharps.py 1834 20120707000000 7d
    
    """
	print(argstr)


def use_missing_data_file():

	hdata = ()

	with open(MISSING_DIR + "/" + MISSING_FILE) as csvFile:
		valueReader = csv.reader(csvFile, delimiter = '\t')
		next(valueReader, None)  # skip the header
		for row in valueReader:
			sharpNum = row[0]
			misstingTimesList = ast.literal_eval(row[1])
			if len(misstingTimesList) > 0:
				hdata = hdata + (sharpNum,)

	pool = Pool(processes = 7)
	pool.map(download_sharps_keysonly2, hdata)


def run_main3():

	
	if len(sys.argv) == 1:
		help()
		exit()

	opt = int(sys.argv[1])

	if opt == 1:
		sn_from = int(sys.argv[2])
		sn_to = int(sys.argv[3])
	
		start = time.time()
	
		i = sn_from
		while i < sn_to:
			sn = str(i)
	
			r = random.randint(4, 8)
			time.sleep(r)
	
			fout = DIR_DL + sn + '.txt'
			download_sharps_keysonly2(sn)
	
			i += 1
	
		end = time.time()
		print("Finished in: %.2f minutes." % ((end - start) / 60.0))
	elif opt == 2:
		use_missing_data_file()
	else:
		print("Invalid option")
		help()
		exit()


if __name__ == "__main__":
	run_main3()

