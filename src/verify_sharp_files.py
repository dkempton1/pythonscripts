import sys
import csv
import os
import glob
import ast
import fnmatch
import pandas as pd

from _datetime import datetime
from bisect import bisect
from multiprocessing import Pool
import traceback

# BASE_DIR = "/home/dustin/Desktop/harps/data"
# BASE_DIR = "/home/dustin/sharps"
BASE_DIR = "/data/SHARPS"
# RAW_FILES_DIR = "/raw-data"
RAW_FILES_DIR = "/raw-sharps"
# HEADER_FILES_DIR = "/headers"
HEADER_FILES_DIR = "/sharps-headers"
# NEW_HEADER_FILES_DIR = "/newheaders"
NEW_HEADER_FILES_DIR = "/new-data"

# OUTPUT_DIR = "/home/dustin/Desktop/harps"
# OUTPUT_DIR = "/home/dustin/sharps"
OUTPUT_DIR = "/home/dkempton"
OUTPUT_FILE = "MissingSharpInfo.csv"

MISSING_TS_FILE = "MissingTSInfo.csv"
WRONGDATA_TS_FILE = "WrongData.csv"


def getRawDirs():
	listOfDirs = os.listdir(BASE_DIR + RAW_FILES_DIR)
	print(listOfDirs)
	for fname in listOfDirs:
		print(fname)
	return listOfDirs


def getHeaderNames():
	files = []
	files.extend(glob.glob(BASE_DIR + HEADER_FILES_DIR + "/*.txt"))
	filesWithExt = {}
	for file in files:
		filesWithExt[file.split("/")[-1].split(".")[0]] = file

	return filesWithExt


def getHeaderNamesWData():

	headers = getHeaderNames()
	headers_w_data = []
	for header in headers:
		header_name = BASE_DIR + HEADER_FILES_DIR + "/" + header + ".txt"
		if len(readFile(header_name)) > 0:
			headers_w_data.append(header)

	return headers_w_data


def getNewHeaderNames():
	files = []
	files.extend(glob.glob(BASE_DIR + NEW_HEADER_FILES_DIR + "/*.csv"))
	filesWithExt = {}
	for file in files:
		filesWithExt[file.split("/")[-1].split(".")[0]] = file

	return filesWithExt


def readFile(fileName):
	values = []
	with open(fileName, newline = "\n") as csvFile:
		valueReader = csv.reader(csvFile, delimiter = '\t')
		next(valueReader, None)
		for row in valueReader:
			values.append(row)
	return values


def parseDTFromNewHeaders(headerValues):

	values = []
	for row in headerValues:
		values.append(datetime.strptime(row[0], "%Y%m%d_%H%M%S"))
	return values


def parseDTFromHeaders(headerValues):

	values = []
	for row in headerValues:
		values.append(datetime.strptime(row[5], "%Y.%m.%d_%H:%M:%S_TAI"))
	return values


def testHeadersVSNew():

	headers = getHeaderNames()
	newHeaders = getNewHeaderNames()
	missing = {}
	for key in headers.keys():
		missingTimes = testHeaderVSNew(headers, newHeaders, key)
		if len(missingTimes) > 0:
			missing[key] = missingTimes

	return missing


def testHeaderVSNew(headers, newHeaders, key):

	missing = []

	headerForKey = readFile(headers[key])

	if key in newHeaders.keys():
		newHeaderForKey = readFile(newHeaders[key])
		listOfNewHeadersTimes = parseDTFromNewHeaders(newHeaderForKey)
		listOfHeadersTimes = parseDTFromHeaders(headerForKey)
		if len(listOfNewHeadersTimes) > 0:
			for time in listOfHeadersTimes:
				ind = bisect(listOfNewHeadersTimes, time, hi = len(listOfNewHeadersTimes) - 1)

				if ind > 0:
					tDelta = time - min(listOfNewHeadersTimes[ind], listOfNewHeadersTimes[ind - 1], key = lambda x: abs(x - time))
				else:
					tDelta = time - listOfNewHeadersTimes[ind]

				if not abs(tDelta.total_seconds()) < 720:
					missing.append(time)
		else:
			for time in listOfHeadersTimes:
				missing.append(time)

	else:
		listOfHeadersTimes = parseDTFromHeaders(headerForKey)
		for time in listOfHeadersTimes:
			missing.append(time)

	return missing


def testForMissingRawDataPoolMethod(sharpTuple):
	return sharpTuple[0], testForMissingRawData(sharpTuple[0], sharpTuple[1])


def testForMissingRawData(sharp, missingTimes):
	timsWithMissingFiles = []

	path = BASE_DIR + RAW_FILES_DIR + "/" + sharp + "/"
	for time in missingTimes:
		timeString = time.strftime("%Y%m%d_%H%M%S")
		files = []
		files.extend(find_files(path, "*" + timeString + "*Bp_err.fits"))
		files.extend(find_files(path, "*" + timeString + "*Bp.fits"))
		files.extend(find_files(path, "*" + timeString + "*Br.fits"))
		files.extend(find_files(path, "*" + timeString + "*Bt.fits"))
		files.extend(find_files(path, "*" + timeString + "*Bt_err.fits"))
		files.extend(find_files(path, "*" + timeString + "*conf_disambig.fits"))
		files.extend(find_files(path, "*" + timeString + "*continuum.fits"))
		files.extend(find_files(path, "*" + timeString + "*Dopplergram.fits"))
		files.extend(find_files(path, "*" + timeString + "*magnetogram.fits"))
		files.extend(find_files(path, "*" + timeString + "*bitmap.fits"))

		if len(files) < 10 :
			timsWithMissingFiles.append(time)

	return timsWithMissingFiles


def find_files(path, path_filter):

	ret_list = []
	for entry in os.scandir(path = path):
		if fnmatch.fnmatch(entry.name, path_filter):
			ret_list.append(entry)
	return ret_list


def help():

	argstr = """
	python verify_sharp_files.py [mode] {args}
		
		1: Compares all SHARP headers with the generated headers and compiles a list of 
		   missing time steps in the raw data.
		2: Compares a single SHARP like above
			args: [SHARP Number to process]
		3: Provides a count of the output in the file produced in 1. Gives the number of
			files missing raw data and the count that are missing calculated time stamps
		4: Cycles through all the SHARP headers and compares the output of the feature 
			calculation in the mvts ouput files.  Some of the parameters calculated are also
			in the headers.  Those that are shall be compared for accuracy. Will also list those
			files that do not have data at every timestep.  
			
	"""
	print(argstr)


def run_main():

	if len(sys.argv) == 1:
		help()
		exit()

	opt = int(sys.argv[1])

	if opt == 1:
		run_all()
	elif opt == 2:
		sharpId = sys.argv[2]
		run_one(sharpId)
	elif opt == 3:
		run_count()
	elif opt == 4:
		run_out_ver()
	elif opt == 5:
		sharpId = sys.argv[2]
		print(test_for_wrong_data(sharpId))
	else:
		print('invalid mode')


def test_for_missing_step(sharp):

	header_name = BASE_DIR + HEADER_FILES_DIR + "/" + sharp + ".txt"
	new_header_name = BASE_DIR + NEW_HEADER_FILES_DIR + "/" + sharp + ".csv"

	cutoffTime = datetime.strptime("2018.05.01_00:00:00", "%Y.%m.%d_%H:%M:%S")

	times_wrong = []
	if os.path.isfile(header_name) and len(readFile(header_name)) > 0 :

		try:
			header_df = pd.read_csv(header_name, sep = '\t')
			if  len(header_df) > 0:

				fileTime = datetime.fromtimestamp(os.path.getmtime(new_header_name))
				if fileTime < cutoffTime:
					new_header_df = pd.read_csv(new_header_name, sep = '\t')

					# if the lengths don't match then it clearly is the old data
					# and needs to be reprocessed.
					if len(new_header_df) != len(header_df):
						print("Missing time step in new header: " + sharp)
						times_wrong.append("All")
					else:
						for i in range(0, len(header_df) - 1):
							time1 = datetime.strptime(header_df.iloc[i]['T_REC'], "%Y.%m.%d_%H:%M:%S_TAI")
							time2 = datetime.strptime(header_df.iloc[i + 1]['T_REC'], "%Y.%m.%d_%H:%M:%S_TAI")

							tDelta = time2 - time1

							if abs(tDelta.total_seconds()) > 721:
								times_wrong.append(time1.strftime("%Y-%m-%d %H:%M:%S"))

		except Exception as e:
			traceback.print_exc()
			return  [sharp, times_wrong]

	return [sharp, times_wrong]


def test_for_wrong_data(sharp):

	header_name = BASE_DIR + HEADER_FILES_DIR + "/" + sharp + ".txt"
	new_header_name = BASE_DIR + NEW_HEADER_FILES_DIR + "/" + sharp + ".csv"

	cutoffTime = datetime.strptime("2018.05.01_00:00:00", "%Y.%m.%d_%H:%M:%S")

	newFile = False
	times_wrong = [[], []]
	if os.path.isfile(header_name)and len(readFile(header_name)) > 0:

		try:
			header_df = pd.read_csv(header_name, sep = '\t')
			if  len(header_df) > 0:

				fileTime = datetime.fromtimestamp(os.path.getmtime(new_header_name))
				if fileTime > cutoffTime:
					newFile = True

				new_header_df = pd.read_csv(new_header_name, sep = '\t')

				# if the lengths don't match then it clearly is the old data
				# and needs to be reprocessed.
				if len(new_header_df) != len(header_df):
					print(len(new_header_df))
					print(len(header_df))
					print("Missing time step in new header: " + sharp)
					if newFile:
						times_wrong[0] = ["All 1"]
					else:
						times_wrong[1] = ["All 1"]
				else:
					#isWrong = False
					for i in range(0, len(header_df)):
						if abs(new_header_df.iloc[i]['TOTUSJH'] - header_df.iloc[i]['TOTUSJH']) > (header_df.iloc[i]['TOTUSJH'] * 0.01) and	abs(header_df.iloc[i]['TOTUSJH']) > 10e-4:

							#if not isWrong:
							#	print("TOTUSJH in " + sharp + " off by: ", abs(new_header_df.iloc[i]['TOTUSJH'] - header_df.iloc[i]['TOTUSJH']))
							#	print("TOTUSJH: ", header_df.iloc[i]['TOTUSJH'])
							#	print("TOTUSJH: ", new_header_df.iloc[i]['TOTUSJH'])
							#	isWrong = True

							if newFile:
								times_wrong[0].append(header_df.iloc[i]['T_REC'])
							else:
								times_wrong[1].append(header_df.iloc[i]['T_REC'])

					if len(header_df) == len(times_wrong[0]) or len(header_df) == len(times_wrong[1]):
						if newFile:
							times_wrong[0] = ["All 2"]
						else:
							times_wrong[1] = ["All 2"]

		except Exception as e:
			traceback.print_exc()
			times_wrong[0] = ["All 2"]
			return [sharp, times_wrong]

	return [sharp, times_wrong]


def run_out_ver():

	outHeader = ["SHARP_ID", "WRONG_TIMES"]

	headers = getHeaderNames()

	pool = Pool(processes = 50)
	missingTSHeaders = pool.map(test_for_missing_step, headers)

	with open(OUTPUT_DIR + "/" + MISSING_TS_FILE, 'w') as csvfile:
		csvout = csv.writer(csvfile, delimiter = '\t')
		csvout.writerow(outHeader)
		for sharpTuple in missingTSHeaders:
			sharpId = sharpTuple[0]
			missingTimes = sharpTuple[1]

			if len(missingTimes) > 0:
				formattedMissingTimes = []
				for time in missingTimes:
					formattedMissingTimes.append(time)
				csvout.writerow([sharpId, formattedMissingTimes])

	wrongDataHeaders = pool.map(test_for_wrong_data, headers)

	with open(OUTPUT_DIR + "/" + WRONGDATA_TS_FILE, 'w') as csvfile:
		csvout = csv.writer(csvfile, delimiter = '\t')
		csvout.writerow(outHeader)
		for sharpTuple in wrongDataHeaders:
			sharpId = sharpTuple[0]
			missingTimes = sharpTuple[1][1]

			if len(missingTimes) > 0:
				formattedMissingTimes = []
				for time in missingTimes:
					formattedMissingTimes.append(time)
				csvout.writerow([sharpId, formattedMissingTimes])

	with open(OUTPUT_DIR + "/NEW_" + WRONGDATA_TS_FILE, 'w') as csvfile:
		csvout = csv.writer(csvfile, delimiter = '\t')
		csvout.writerow(outHeader)
		for sharpTuple in wrongDataHeaders:
			sharpId = sharpTuple[0]
			missingTimes = sharpTuple[1][0]

			if len(missingTimes) > 0:
				formattedMissingTimes = []
				for time in missingTimes:
					formattedMissingTimes.append(time)
				csvout.writerow([sharpId, formattedMissingTimes])


def run_count():

	hdata = ()
	hdata2 = ()
	with open(OUTPUT_DIR + "/" + OUTPUT_FILE) as csvFile:
		valueReader = csv.reader(csvFile, delimiter = '\t')
		next(valueReader, None)  # skip the header
		for row in valueReader:
			sharpNum = row[0]
			misstingTimesList = ast.literal_eval(row[1])
			if(len(misstingTimesList) > 0):
				hdata = hdata + (sharpNum,)
			else:
				hdata2 = hdata2 + (sharpNum,)

	print(len(hdata))
	print(len(hdata2))


def run_all():

	outHeader = ["SHARP_ID", "MISSING_RAW_AT_TIMES" ]

	headersWithMissingData = testHeadersVSNew()
	print("NumSharpsMssing: " + str(len(headersWithMissingData)))

	pool = Pool(processes = 30)
	missingRawList = pool.map(testForMissingRawDataPoolMethod,
							list(zip(headersWithMissingData.keys(),
									[headersWithMissingData[key] for key in headersWithMissingData])))

	with open(OUTPUT_DIR + "/" + OUTPUT_FILE, 'w') as csvfile:
		csvout = csv.writer(csvfile, delimiter = '\t')
		csvout.writerow(outHeader)
		for sharpTuple in missingRawList:
			sharpId = sharpTuple[0]
			missingTimes = sharpTuple[1]

			if len(missingTimes) > 0:
				formattedMissingTimes = []
				for time in missingTimes:
					formattedMissingTimes.append(time.strftime("%Y-%m-%d %H:%M:%S"))
				csvout.writerow([sharpId, formattedMissingTimes])
			else:
				csvout.writerow([sharpId, "[]"])


def run_one(sharpId):

	outHeader = ["SHARP_ID", "MISSING_RAW_AT_TIMES" ]

	headers = getHeaderNames()
	newHeaders = getNewHeaderNames()
	headersWithMissingData = {}
	headersWithMissingData[sharpId] = testHeaderVSNew(headers, newHeaders, sharpId)

	with open(OUTPUT_DIR + "/" + sharpId + OUTPUT_FILE, 'w') as csvfile:
		csvout = csv.writer(csvfile, delimiter = '\t')
		csvout.writerow(outHeader)
		for missingHeader in headersWithMissingData:
			timesMissing = testForMissingRawData(missingHeader, headersWithMissingData[missingHeader])
			if len(timesMissing) > 0:
				formattedMissingTimes = []
				for time in timesMissing:
					formattedMissingTimes.append(time.strftime("%Y-%m-%d %H:%M:%S"))
				csvout.writerow([missingHeader, formattedMissingTimes])
			else:
				csvout.writerow([missingHeader, "[]"])


if __name__ == "__main__":
	run_main()

