
import sys, os, time
import urllib, json
import pdb
import drms

import pandas as pd, numpy as np
from datetime import timedelta, datetime, timezone

def daterange(start_date, end_date):
	for n in range(int ((end_date - start_date).days/15)):
		yield start_date + timedelta(n*15)

def download_flares():
	 
	from sunpy.time import TimeRange
	import sunpy.instr.goes


	flare_class = 'A1'
	ofile		 = 'allFlares.txt'
	ofile2		 = '2010-2017-C1plus-harps.txt'



	start_date = datetime(2010, 1, 1, tzinfo=timezone.utc)
	end_date = datetime(2017, 12, 10, tzinfo=timezone.utc) 

	listofresults = []	

	for single_date in daterange(start_date, end_date):
		time_range = TimeRange(single_date, single_date + timedelta(15))
		listofresults = listofresults + (sunpy.instr.goes.get_goes_event_list(time_range, flare_class))
		print(single_date.strftime("%y-%m-%d"))

	 
	print('total results: ', len(listofresults))

	df = pd.DataFrame(listofresults)
	df.to_csv(ofile, sep='\t', index=False)
	 

	 
	 
########################################################################
def run_main():
	 
	 
	start = time.time()
	 
	download_flares()
	 
	end = time.time()

	print("Finished in: %.2f minutes." % ( (end-start)/60.0) )
	 


if __name__ == "__main__":
	 run_main()
	 
