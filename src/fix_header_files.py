import sys
import os
import glob
import pandas as pd

from multiprocessing import Pool
import traceback

# BASE_DIR = "/home/dustin/Desktop/harps/data"
# BASE_DIR = "/home/dustin/sharps"
BASE_DIR = "/data/SHARPS"

NEW_HEADER_FILES_DIR = "/new-data2"


def getNewHeaderNames():
	files = []
	files.extend(glob.glob(BASE_DIR + "/" + NEW_HEADER_FILES_DIR + "/*.csv"))
	filesWithExt = {}
	for file in files:
		filesWithExt[file.split("/")[-1].split(".")[0]] = file

	return filesWithExt

def is_non_zero_file(fpath):
	return os.path.isfile(fpath) and os.path.getsize(fpath) > 0




def run_main():

	outHeader = ["SHARP_ID", "WRONG_TIMES"]

	headers = getNewHeaderNames()

	pool = Pool(processes = 50)
	missingTSHeaders = pool.map(fix_for_wrong_header, headers)




def fix_for_wrong_header(sharp):
	new_header_name = BASE_DIR + NEW_HEADER_FILES_DIR + "/" + sharp + ".csv"
	
	try:
		if(is_non_zero_file(new_header_name)):
			calc_header = pd.read_csv(new_header_name, sep = '\t', header=0)
			if calc_header.shape[1] == 38:
				calc_header.columns = ['Timestamp', 'TOTUSJH', 'TOTBSQ', 'TOTPOT', 'TOTUSJZ', 'ABSNJZH', 'SAVNCPP',
					'USFLUX', 'TOTFZ', 'MEANPOT', 'EPSZ', 'MEANSHR', 'SHRGT45', 'MEANGAM', 'MEANGBT',
					'MEANGBZ', 'MEANGBH', 'MEANJZH', 'TOTFY', 'MEANJZD', 'MEANALP', 'TOTFX', 'EPSY',
					'EPSX', 'R_VALUE', 'RBZ_VALUE', 'RBT_VALUE', 'RBP_VALUE', 'FDIM', 'BZ_FDIM',
					'BT_FDIM', 'BP_FDIM', 'CRVAL1', 'CRLN_OBS', 'LAT_MIN', 'LON_MIN', 'LAT_MAX', 'LON_MAX']
			elif calc_header.shape[1] == 34:
				calc_header.columns = ['Timestamp', 'TOTUSJH', 'TOTBSQ', 'TOTPOT', 'TOTUSJZ', 'ABSNJZH', 'SAVNCPP',
					'USFLUX', 'TOTFZ', 'MEANPOT', 'EPSZ', 'MEANSHR', 'SHRGT45', 'MEANGAM', 'MEANGBT',
					'MEANGBZ', 'MEANGBH', 'MEANJZH', 'TOTFY', 'MEANJZD', 'MEANALP', 'TOTFX', 'EPSY',
					'EPSX', 'R_VALUE', 'RBZ_VALUE', 'RBT_VALUE', 'RBP_VALUE', 'FDIM', 'BZ_FDIM',
					'BT_FDIM', 'BP_FDIM', 'CRVAL1', 'CRLN_OBS']

			with open(new_header_name, 'w') as out_file:
				calc_header.to_csv(out_file, sep = '\t', index = False)

	except Exception as e:
		traceback.print_exc()
		return None
	return None


if __name__ == "__main__":
	run_main()

